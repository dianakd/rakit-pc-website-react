import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import "./Auth.css";
import "./Auth.css";
import Logo from "../../img/RPC_final.png";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";


function UserSignup () {
  const [NamaUser, setNameuser] = useState("");
  const [Password, setPassword] = useState("");

  const [passwordVisible, setPasswordVisible] = useState(false);

  async function signup(){
    console.warn(NamaUser, Password)
    const formData = new FormData();
    formData.append('NamaUser', NamaUser);
    formData.append('Password', Password);
    let result = await fetch("http://103.105.78.75/api/Forum/Upload",{
      method: 'POST',
      body: formData
    });
    result = await result.json();
    console.log(result);
    
  }

  return (
    <div style={{background: '#f3f3f3'}}>
      <div className="blur" style={{top: '-18%', right: '0'}}></div>
      <div className="blur" style={{top: '36%', left: '-8rem'}}></div>

      <div className="Auth">
        <div className="a-left">
          <img src={Logo} alt="" />
          <div className="Webname">
            <h1>Rakit PC</h1>
            <h6>Rakit PC anda disini</h6>
          </div>
        </div>

        <div className="a-right">
          <form className="infoForm authForm">
            <h3>Sign up</h3>

            <div>
              <input
                type="text"
                placeholder="Username"
                className="infoInput"
                onChange={(e) => setNameuser(e.target.value)}
              />
            </div>

            <div style={{gap: ''}}>
              <input
                // style={{position: 'absolute'}}
                maxLength={999} 
                type={passwordVisible ? "text" : "password"}
                className="infoInput"
                id="Password"
                value={Password}
                name="Password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <div
                className="showHidePassword"
                onClick={() => setPasswordVisible(!passwordVisible)}
              >
                {passwordVisible ? (
                  <VisibilityOffOutlinedIcon />
                ) : (
                  <VisibilityOutlinedIcon />
                )}
              </div>
            </div>

            <div style={{gap: '0',  paddingBottom: '0', marginBottom: '0'}}>
                <span style={{fontSize: '12px'}}>Already have an account. <Link to={'/forum/Login'}>Login!</Link></span>
            </div>
            
            <Link to={'/forum/Login'}>
              <button className="button infoButton" type="submit">Signup</button>
            </Link>
          </form>
        </div>

      </div>
    </div>
  );
};




export default UserSignup;

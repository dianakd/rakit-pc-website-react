import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import "./Auth.css";
import "./Auth.css";
import Logo from "../../img/RPC_final.png";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";



function UserLogin ()  {
  const [NamaUser, setNameuser] = useState("");
  const [Password, setPassword] = useState("");

  const [passwordVisible, setPasswordVisible] = useState(false);

  async function login(){
    console.warn(NamaUser, Password)
    const formData = new FormData();
    formData.append('NamaUser', NamaUser);
    formData.append('Password', Password);
    let result = await fetch("http://103.105.78.75/api/Forum/Upload",{
      method: 'POST',
      body: formData
    });
    result = await result.json();
    console.log(result);
    
  }
  return (
    <div style={{background: '#f3f3f3'}}>
      <div className="blur" style={{top: '-18%', right: '0'}}></div>
      <div className="blur" style={{top: '36%', left: '-8rem'}}></div>

      <div className="Auth">
        <div className="a-left">
          <img src={Logo} alt="" />
          <div className="Webname">
            <h1>Rakit PC</h1>
            <h6>Rakit PC anda disini</h6>
          </div>
        </div>

        <div className="a-right">
          <form className="infoForm authForm">
            <h3>Log In</h3>
    
            <div>
              <input
                type="text"
                placeholder="Username"
                className="infoInput"
                onChange={(e) => setNameuser(e.target.value)}
            
              />
            </div>
    
            <div style={{gap: ''}}>
              <input
                // style={{position: 'absolute'}}
                maxLength={999} 
                type={passwordVisible ? "text" : "password"}
                className="infoInput"
                id="Password"
                value={Password}
                name="Password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <div
                className="showHidePassword"
                onClick={() => setPasswordVisible(!passwordVisible)}
              >
                {passwordVisible ? (
                  <VisibilityOffOutlinedIcon />
                ) : (
                  <VisibilityOutlinedIcon />
                )}
              </div>
            </div>
    
            <div>
                <span style={{ fontSize: "12px" }}>
                  Don't have an account <Link to={'/forum/Signup'}>Sign Up</Link>
                </span>
              <Link to={'/forumpage'}>
              <button className="button infoButton">Login</button>
              {/* <button className="button infoButton" onClick={this.handleRegisterSubmit}>Login</button> */}
              </Link>
            </div>
          </form>
        </div>

      </div>
    </div>
  );
};



export default UserLogin;

import React, { useEffect, useState } from 'react'
import PostSide from '../../components/PostSide/PostSide'
import ProfileSide from '../../components/profileSide/ProfileSide'
import RightSide from '../../components/RightSide/RightSide'
import './ForumPage.css'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


const ForumPage = () => {

  const url = "http://103.105.78.75/api/Profile/Check/47";
  const [respond, setRespond] = useState()

  const getDataProfil = async () => {
    const response = await fetch(url);
    const dataProfil = await response.json();
    setRespond(dataProfil);
  };

  useEffect(() => {
    getDataProfil();
  }, []);
  console.log('ini respond: ', respond);

  return (
    <>
      { respond !== undefined &&
        <>
          {
            respond == 1 ?
            <div className="ForumPage">
              <ProfileSide/>
              <PostSide/>
              <RightSide/>
            </div>
            :
            <>
              {window.location.replace('/isiform')}
            </>
          }
        </>
      }
    </>
  )
}

export default ForumPage
import React from 'react';
import PostSide from '../../components/PostSide/PostSide'
import ProfileCard from '../../components/ProfileCard.jsx/ProfileCard'
import ProfileLeft from '../../components/ProfileLeft/ProfileLeft'
import RightSide from '../../components/RightSide/RightSide'
import LogoSearch from '../../components/LogoSearch/LogoSearch'
import './profile.css';

const Profile = () => {
  return (
    <div>
      <div className="blur" style={{top: '-18%', right: '0'}}></div>
      <div className="blur" style={{top: '36%', left: '-8rem'}}></div>

      <div className="Profile">

        <LogoSearch/>

        <div className="Profile-center" style={{margin: '1rem'}}>
            <ProfileCard/>
            {/* <PostSide/> */}
        </div>

        <RightSide />
      </div>
    </div>
  )
}

export default Profile
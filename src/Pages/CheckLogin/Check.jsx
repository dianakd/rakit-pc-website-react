import "./Check.css";
import React, { useState } from "react";
import ProgressLine from "./ProgressLine";
import Button from '@mui/material/Button';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {RemoveScroll} from 'react-remove-scroll';
import { Link } from "react-router-dom";


const ProgressBar = ({ width, percent, status }) => { 
  const [value, setValue] = React.useState(0);

  React.useEffect(() => {
    setValue(percent * width);
  });

  async function checkProfile(id) {
    let result = await fetch("http://103.105.78.75/api/Profile/Check/" + id, {
      method: "GET",
    });
    result = await result.json();
    console.warn(result);
    window.location.reload();
  }

  return (
    <>
    
      <div className="Check">
        <div className="Checkbox" >
          <ProgressLine
            visualParts={[
              {
                percentage: "90%",
                color: "#7a77ff"
              }
            ]}
          />
          <Row className="checkMobile">
            <div className="Text">
              <h4> Apakah anda yakin untuk lanjut?</h4>
            </div>
            <div className="tombol">
              <Button style={{ margin: "10px" }} variant="contained" color="success">
                Ya
              </Button>
              {/* <Link to={'/'}> 
                <Button variant="contained" color="error">
                  Tidak
                </Button>
              </Link> */}
            </div>
          </Row>
        </div>
      </div>
      
    </>
  );
};


export default ProgressBar;
import React, { useState, useEffect } from "react";
import "./Post.css";
import { Icon } from "@iconify/react";
import { User, Grid, Card, Text } from "@nextui-org/react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Menu } from "@mantine/core";
import Checkbox from '@mui/material/Checkbox';
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import Favorite from '@mui/icons-material/Favorite';
import { MdFavorite, MdFavoriteBorder } from "react-icons/md";
import { A200 } from '@mui/material/colors';
import {
  IconSettings,
  IconSearch,
  IconPhoto,
  IconMessageCircle,
  IconTrash,
  IconArrowsLeftRight,
  IconUserCircle
  
} from "@tabler/icons";

function PostNew() {
  const url = "http://103.105.78.75/api/Forum/List";
  const [postdata, setPostdata] = useState([]);

  const [commentOpen, setCommentOpen] = useState(false);

  useEffect(() => {
    getDataPost();
  }, []);

  async function getDataPost() {
    let result = await fetch("http://103.105.78.75/api/Forum/List");
    result = await result.json();
    setPostdata(result.reverse());
    console.log(result);
  }

  return (
    <div>
      {postdata.map((item) => {
        return (
          <CardPost
            key={item.IdPost}
            idPost={item.IdPost}
            id={item.IdPengepost}
            nama={item.NamaPengepost}
            judul={item.JudulPost}
            isi={item.IsiPost}
            tgl={item.created_at}
            image={item.img_path}
            like={item.Like}
            tipe={item.TipePost}
          />
        );
      })}
    </div>
  );
}

/** =============== Card Post Component =============== */

/** funtion request comment data. ditaruh di luar component function agar:
 * nggk memberatkan render cycle
 * tidak di deklarasikan lagi tiap rerender (stable reference) */
const getCommentByPostId = async (id) => {
  const response = await fetch(`http://103.105.78.75/api/Comment/Post/${id}`)
    .then((res) => res.json())
    .catch((err) => console.log(err));

  return response;
};

function CardPost(props) {
  const [title, setTitle] = useState("");
  const [IdPostComment, setIdPostComment] = useState("");
  const [IdPengcomment, setIdPengcomment] = useState("");
  const [NamaPengcomment, setNamaPengcomment] = useState("");
  const [IsiComment, setIsiComment] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState();
  const [validationError, setValidationError] = useState({});
  const [show, setShow] = useState(false);
  const [comments, setComments] = useState([]);

  const { idPost } = props;

  const label = { inputProps: { 'aria-label': 'Checkbox demo' } };


  useEffect(() => {
    getCommentByPostId(idPost).then((res) => setComments(res));
  }, [idPost]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [favorite, setFavorite] = useState(false);

  const toggleFavorite = () => setFavorite((prev) => !prev);


  const { loading = false } = props;

  const changeHandler = (event) => {
    setImage(event.target.files[0]);
  };

  const createComment = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("title", title);
    formData.append("IdPostComment", IdPostComment);
    formData.append("IdPengcomment", IdPengcomment);
    formData.append("NamaPengcomment", NamaPengcomment);
    formData.append("IsiComment", IsiComment);
    formData.append("description", description);
    formData.append("image", image);

    let result = await fetch("http://103.105.78.75/api/Comment/Upload", {
      method: "POST",
      body: formData,
    });
    alert("Komenn Terkirim");
    window.location.reload();
  };

  async function deletePost(id) {
    let result = await fetch("http://103.105.78.75/api/Forum/Delete/" + id, {
      method: "GET",
    });
    result = await result.json();
    console.warn(result);
    window.location.reload();
  }

  async function addLike(id) {
    let result = await fetch(
      "http://103.105.78.75/api/Forum/List/TambahLike/" + id,
      {
        method: "GET",
      }
    );
    result = await result.json();
    console.warn(result);
    window.location.reload();
  }

  return (
    <>
      <div className="Post" style={{ gap: "2rem" }}>
        <div className="container">
          <Row>
            <div className="userDekstop">
              <Row style={{marginTop: "5px", paddingTop: "10px"}}>
                  <Col style={{flex: "0 0"}}>
                    <Icon icon="healthicons:ui-user-profile" style={{marginLeft: "20px"}} color="#cfcdcd" height="50"/>
                  </Col>
                  <Col style={{marginTop: "7px", paddingInline:"0"}}>
                    <Text h5 css={{ lineHeight: "$xs" }} style={{marginBottom: "0"}}>
                    {props.nama}
                    </Text>
                    <Text size={12}  css={{ color: "#7a77ff" }}>{props.tipe}</Text>
                  </Col>
                  <Col sm={4} className="titiktiga">
                    <Menu position="right-start" shadow="md" width={170}>
                      <Menu.Target>
                        <Icon
                          className="float-end "
                          style={{ marginTop: "10px", marginRight: "10px"}}
                          icon="ci:more-vertical"
                          color="#7a77ff"
                          width="30"
                        />
                      </Menu.Target>

                      <Menu.Dropdown>
                        <Menu.Item
                          color="red"
                          onClick={() => deletePost(props.idPost)}
                          icon={<IconTrash size={14} />}
                        >
                          Hapus post saya
                        </Menu.Item>
                      </Menu.Dropdown>
                    </Menu>
                  </Col>
                </Row>
              </div>
                
              
          </Row>

          <Row>
            <div className="content">
              <h4 style={{marginBottom: "5px"}}>{props.judul}</h4>
              <span>{props.isi}</span>
              <img src={`http://103.105.78.75/${props.image}`} alt="" />

              <div className="postReact">
                <div>
                  {/* <Checkbox  {...label } size="large" onClick={() => addLike(props.idPost)} icon={<FavoriteBorder sx={{ color: '#7a77ff' }} />} checkedIcon={<Favorite sx={{ color: '#7a77ff' }} />} /> */}

                  <Checkbox  {...label } size="large" onClick={() => addLike(props.idPost)} icon={<FavoriteBorder sx={{ color: '#7a77ff' }} />} checkedIcon={<FavoriteBorder sx={{ color: '#7a77ff' }} />} />
                  
                  {/* <div onClick={toggleFavorite} className="top-rated-car-react-button">
                    {favorite ? (
                      <MdFavoriteBorder style={{ color: "#7a77ff" }} />
                    ) : (
                      <MdFavorite style={{ color: "#7a77ff" }} />
                    )}
                  </div> */}

                  <span
                    style={{
                      paddingTop: "0",
                      color: "var(--gray)",
                      fontSize: "12px",
                      marginLeft: "10px",
                    }}
                  >
                    {props.like} Likes
                  </span>
                  {/* if not like <Icon icon="icon-park-outline:like" color="#7a77ff" height="30" /> */}
                </div>

                <div style={{padding: "10px"}} onClick={handleShow}>
                  <Icon icon="bx:comment-detail" color="#7a77ff" height="30" />
                  <span
                    style={{
                      color: "var(--gray)",
                      fontSize: "12px",
                      marginLeft: "10px",
                    }}
                  >
                   Comments
                  </span>
                </div>
              </div>

              {/* <div className="Comment">
                <span>
                  <b>{props.nama}</b>
                </span>
                <span> {props.judul}</span>
              </div> */}

              {/* check ada comment atau tidak. kalau ada baru ditampilkan. */}
              {comments && comments.length > 0
                ? comments.map((comment) => (
                    <div className="Comment" key={comment.IdComment}>
                      <Comment
                        id={comment.IdComment}
                        username={comment.NamaPengcomment}
                        content={comment.IsiComment}
                      />
                    </div>
                  ))
                : null}
            </div>
          </Row>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Comment</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {Object.keys(validationError).length > 0 && (
            <div className="row">
              <div className="col-12">
                <div className="alert alert-danger">
                  <ul className="mb-0">
                    {Object.entries(validationError).map(([key, value]) => (
                      <li key={key}>{value}</li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          )}
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Id Pengcomment</Form.Label>
              <Form.Control
                type="text"
                placeholder="IdComment"
                autoFocus
                value={IdPengcomment}
                onChange={(event) => {
                  setIdPengcomment(event.target.value);
                }}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Id PostComment</Form.Label>
              <Form.Control
                type="text"
                placeholder="IdPostComment"
                value={IdPostComment}
                onChange={(event) => {
                  setIdPostComment(event.target.value);
                }}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nama Pengoment</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nama anda adalah..."
                value={NamaPengcomment}
                onChange={(event) => {
                  setNamaPengcomment(event.target.value);
                }}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Ketik komen anda disini..</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={IsiComment}
                onChange={(event) => {
                  setIsiComment(event.target.value);
                }}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={createComment}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
/** =============== Comment Component =============== */
function Comment(props) {
  const { id, username, content } = props;

  return (
    <div>
      <span>
        <b>{username}</b>
      </span>
      <span> {content}</span>
      {/* delete comment */}
      {/* ..... */}
    </div>
  );
}

export default PostNew;

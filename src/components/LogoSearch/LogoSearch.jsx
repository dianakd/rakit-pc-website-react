import React from 'react'
import Logo from '../../img/RPC_final.png'
import {UilSearch} from '@iconscout/react-unicons'
import { User } from "@nextui-org/react";
import './LogoSearch.css'
const LogoSearch = () => {
  return (
   <div className="LogoSearch">
        <User.Link color="#7a77ff" href="https://play.google.com/store/apps/details?id=com.rakitpc.rakitpc">
            <img src={Logo}  width={40} height={40}  alt="" />
            <img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' width={128}/>
        </User.Link>
       {/* <div className="Search">
           <input type="text" placeholder='#Explore' />
           <div className="s-icon">
               <UilSearch/>
           </div>
       </div> */}
   </div>
  )
}

export default LogoSearch
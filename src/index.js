import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ReactDOM from "react-dom/client";
import "./index.css";
import "./Pages/ForumPage/ForumPage.css";
import App from "./App";
import Forum from "./Forum";
// import ForumPage from "./Pages/ForumPage/ForumPage";
import Profile from "../src/Pages/Profile/Profile";
import Auth from "../src/Pages/Auth/Auth";
import Login from "./Pages/Auth/UserLogin";
import Signup from "./Pages/Auth/UserSignup"
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.css';
import DataProfile from "../src/DataProfile";
import IsiForm from "../src/IsiForm";


axios.defaults.baseURL = "http://127.0.0.1:8000/";
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';

axios.defaults.withCredentials = true;

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Router>
    <Switch>
      {/* <Route exact path="/" component={App} />
      <Route path="/forum" component={Forum} /> */}
      {/* <Route path="/forumpage" component={ForumPage} />
      // <Route path="/forum/Auth" component={Auth} />
      // <Route path="/forum/login" component={Login} />
      // <Route path="/forum/signup" component={Signup} />
      // <Route path="/forum/profile" component={Profile} />

      // <Route path="/dataprofile" component={DataProfile} />
      // <Route path="/isiform" component={IsiForm} /> */}
      
      <Switch>
        <Route exact path="/">
          <App />
        </Route>
        <Route path="/forum/Login">
          <Login/>
        </Route>
        <Route path="/forum/SignUp">
          <Signup/>
        </Route>
        <Route path="/forumpage">
          <Forum/>
        </Route>
        <Route path="/forum/profile">
          <Profile/>
        </Route>
      </Switch>
    </Switch>
  </Router>
  // <React.StrictMode>
  //   {/* <App /> */}
  //   <Forum />
  // </React.StrictMode>
);
